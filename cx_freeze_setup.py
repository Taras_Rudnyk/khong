# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import sys
from cx_Freeze import setup, Executable

build_exe_options = {}
base = None

if sys.platform == 'win32':
    base = 'Win32GUI'

setup(
    name='khong',
    version='0.1',
    description='GUI for your Fabric script',
    options={'build_exe': build_exe_options},
    executables=[Executable('app.py', icon='icon.ico')]
)
