# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import py2exe

from distutils.core import setup


setup(
    windows=['app.py'],
    options={
        'py2exe': {
            'includes': ['sip']
        }
    }
)
