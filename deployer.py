# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
try:
    from fabric.api import env, run, put, cd, local
    env.shell = 'bash -c'
except ImportError:
    pass

def restart_app(appname):
    """
    Tell supervisor to restart application
    """
    return 0


def pack():
    """
    Create archive of all projects files and return resulting name
    """
    print(env.host, env.user, env.password, env.shell)


def deploy():
    """
    Delpoy project to remote server
    """
    run('uname -a')