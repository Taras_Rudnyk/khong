# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import os
import subprocess

from PyQt5 import QtCore

from app import logger


class Worker(QtCore.QThread):
    def __init__(self, parent=None, *, fab_conf, index):
        QtCore.QThread.__init__(self, parent)
        self.result = None
        self.processed = False
        self.fab_conf = fab_conf
        self.failed = False
        self.index = index

    def run(self):
        conf = self.fab_conf
        logger.info('Trying to execute fabric command in worker with params: %s' % conf)
        try:
            os.chdir(conf['cdir'])  # Current directory the folder where deployer file is placed
            output = subprocess.check_output([
                r'C:\Python27\Scripts\fab.exe', conf['command'],
                '--fabfile', conf['deployer'],
                '--host', conf['host'],
                '--user', conf['user'],
                '--password', conf['password']
            ], stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            logger.error(e.output.decode())
            self.failed = True
        else:
            self.result = output.decode()
