# -*- coding: utf-8 -*-
__author__ = 'Most Wanted'
import os
import hashlib

from PyQt5 import QtWidgets

from app import logger
from ui import info_dialog, add_dialog


class RemoteServer(object):
    def __init__(self, name, host, user_name, user_pass):
        self.name = name
        self.host = host
        self.user_name = user_name
        self.user_pass = user_pass

    def __str__(self):
        #pass_asterisk = '*' * len(self.user_pass)
        return '{} [{}@{}:22]'.format(self.name,
                                         self.user_name,
                                         self.host)

    def __repr__(self):
        return self.__str__()

    def serialize(self):
        name_md5 = hashlib.md5(str(self.__dict__).encode()).hexdigest()
        return {
            name_md5: {
                'host': self.host,
                'name': self.name,
                'user': self.user_name,
                'pass': self.user_pass
            }
        }


class AppConfig(object):
    def __init__(self, deployer_path, servers=None):
        self.deployer_path = deployer_path

        if servers is None:
            self.servers = []
        else:
            self.servers = servers

    @property
    def path_from_frozen(self):
        # Assuming that deployer file is in the same folder as main.py file
        current_script = __file__
        """
        if hasattr(sys, 'frozen'):
            current_script = sys.executable
            logger.debug('Running from cx_Freeze exe file, %s' % current_script)
        else:
            current_script = sys.argv[0]
            logger.debug('Running as python script %s' % current_script)
        current_dir = os.path.abspath(os.path.dirname(current_script))
        full_path = os.path.join(current_dir, self.deployer_py)

        return full_path
        """
        return current_script

    @property
    def deployer_dir(self):
        return os.path.dirname(self.deployer_path)

    @property
    def deployer_file(self):
        return os.path.basename(self.deployer_path)

    @property
    def deployer_module(self):
        deployer_file = os.path.basename(self.deployer_path)
        if deployer_file.endswith('.py'):
            name, ext = os.path.splitext(deployer_file)
            return name
        else:
            logger.error('Incorrect deployer file. Not a python file: [%s] at [%s]' % (deployer_file,
                                                                                       self.deployer_path))
            raise ValueError('Incorrect deployer file')


class AddServerDialog(add_dialog.Ui_Dialog):
    pass

def show_info_dialog(message, level=None):
    u = info_dialog.Ui_Dialog()
    dialog = QtWidgets.QDialog()
    u.setupUi(dialog)
    dialog.setFixedSize(dialog.size())
    # todo: set icon in dialog based on message level
    u.info_label.setText(message)
    dialog.show()
    dialog.exec()
